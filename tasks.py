import sys

from invoke import task

@task
def clean(c):
    for path in ('build', 'dist', 'src/*.egg-info'):
        c.run(f'rm -rf {path}')

@task(clean)
def build(c):
    c.run('python setup.py bdist_wheel')

@task
def utest(c):
    c.run('python -m unittest discover -s tests/utest',
          replace_env=True,
          env={'PYTHONPATH': 'src'})

@task
def atest(c, in_ci=False):
    cmd = 'python -m robot -x junit.xml tests/atest'
    if in_ci:
        c.run(cmd)
    else:
        c.run(cmd, env={'PYTHONPATH': 'src'}, pty=True)
